//
//  TechLogTests.m
//  TechLogTests
//
//  Created by Peter Facey on 18/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogTests.h"
#import "FSTech01Parser.h"

@implementation TechLogTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

-(void)testFSTech01ParserSubStringOfString
{
    NSString *testString = @"1234567890";
    
    FSTech01Parser *parser = [FSTech01Parser parser];
    
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:1 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:2 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:3 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:4 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:5 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:6 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:7 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:8 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:9 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:10 withTrimmedResult:YES]);
    NSLog(@"TEST: %@", [parser subStringOfString:testString startIndex:-1 range:12 withTrimmedResult:YES]);
    
}

@end
