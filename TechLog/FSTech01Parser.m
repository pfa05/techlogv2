//
//  FSTech01Parser.m
//  TechLog
//
//  Created by Peter Facey on 18/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//
// Imports TECH01 Tech log reports

#import "FSTech01Parser.h"
#import "MBProgressHUD.h"
#import "TechLogCoupon+Customisation.h"
#import "TechLogEntry+Customisation.h"
#import "TechLogPart+Customisation.h"
#import "TechLogEntry.h"
#import "FSManagedDataManager.h"
#import <unistd.h>
#import <dispatch/dispatch.h>
#import "FSDiagnosisAssistant.h"
#import "FSAppDelegate.h"

@implementation FSTech01Parser

static FSTech01Parser *_parser;

@synthesize unimportedTechLogEntries = _unimportedTechLogEntries;

#pragma mark Singleton Methods
+(FSTech01Parser *) parser
{
    if (_parser == nil)
    {
        _parser = [[FSTech01Parser alloc] init];
    }
    return _parser;
}

#pragma mark Import Methods
//
// Tries to import a given tech log report
//
-(void) importFromURL:(NSURL *)url
{
    if(url != nil && [url isFileURL])
    {
        
        UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        UIView *rootView = rootViewController.view;
        
        [self importFromURL:url onView:rootView];
    }
}

/*
 Imports the file passed in as the url in a block.
 The method will display a MBProgressHUD on the view if presented.
 */
-(void) importFromURL:(NSURL *)url onView:(UIView *) view
{
    if ([url isFileURL])
    {
        FSDiagnosisAssistant *diagnosticsAssistance = FSDiagnosisAssistant.assistant;
        
        dispatch_queue_t queue = dispatch_queue_create("au.com.flightscape", DISPATCH_QUEUE_SERIAL);
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
        [view addSubview:hud];
        hud.labelText = @"Initialising";
        hud.minSize = CGSizeMake(135.f, 135.f);
        hud.dimBackground = YES;
        
        [hud showAnimated:YES whileExecutingBlock:^{[self importFromURL:url onHUD:hud];}
                              onQueue:queue
          completionBlock:
              ^{
                  [hud removeFromSuperview];
                  // Check for errors.
                  if (self.unimportedTechLogEntries !=nil && self.unimportedTechLogEntries.count > 0)
                  {
                      diagnosticsAssistance.unimportedTechLogEntries = self.unimportedTechLogEntries;
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Import Issues"
                                                                          message:@"There were some issues importing the latest tech log report. Please send email to developer to improve this product."
                                                                         delegate:diagnosticsAssistance cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Send Email",nil];
                      [alertView performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                  }
              }];
        
    }
}

-(void) importFromURL:(NSURL *)url onHUD:(MBProgressHUD *)hud
{
    @try {
        self.unimportedTechLogEntries = [[NSMutableArray alloc] init];
        if(url != nil && [url isFileURL])
        {
            // 1. Load the file into an array of strings.
            NSError *urlReadError = nil;
            NSStringEncoding usedEncoding;
            
            NSString *urlContents = [NSString stringWithContentsOfURL:url usedEncoding:&usedEncoding error:&urlReadError];
            NSCharacterSet *newlineCharacterSet = [NSCharacterSet newlineCharacterSet];
            NSArray *contentAsLines = [urlContents componentsSeparatedByCharactersInSet:newlineCharacterSet];
            
            if (contentAsLines.count == 0)
            {
                hud.hidden = YES;
                dispatch_sync(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Tech Log Load" message:@"The tech log report was empty. No tech log entries have been loaded." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];});
                return;

            }
            
            // 2. Check the file is a TECH01 report
            NSString *firstLine = (NSString*) contentAsLines[0];
            NSString *tech01Field = [firstLine substringWithRange:NSMakeRange(0, 7)];
            if ([tech01Field caseInsensitiveCompare:@"1TECH01"] != NSOrderedSame)
            {
                hud.hidden = YES;
                dispatch_sync(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Tech Log Load" message:@"Unable to import selected file. It is not a correctly formatted TECH01 report." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];});
                return;
            }
            
            // 3. Get the runTime of the report
            NSString *reportRunDateTimeString = [self subStringOfString:firstLine startIndex:105 range:16 withTrimmedResult:YES];
            //NSLog(@"%@", reportRunDateTimeString);
            NSDateFormatter *runDateTimeFormatter = [[NSDateFormatter alloc] init];
            //[runDateTimeFormatter setDateFormat:@"dd MM yyyy.hh.mm.ss ZZZZ"];
            [runDateTimeFormatter setDateFormat:@"dd MM yyyy hh:mm"];
            [runDateTimeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Australia/Sydney"]];
            NSDate *reportRunDateTime = [runDateTimeFormatter dateFromString:reportRunDateTimeString];
            //NSLog(@"%@", [reportRunDateTime debugDescription]);

            // Strip the file contents into tech log entries. Each tech log entry will be stored as an array of string stored within an encompassing array
            NSMutableArray *techLogEntries = [[NSMutableArray alloc] init];
            NSMutableArray *currentTechLogEntry = nil;
            
            // Create an array of lines in the file that are not specifically related to content of a tech log entry.
            // These lines are usually page formatting lines or meta data about the report.
            NSArray *excludedLines = [NSArray arrayWithObjects:
                                     @"1TECH01                                           Q A N T A S    A I R W A Y S    L T D.",
                                     @"TECHNICAL  LOG  DEFECTS  FOR",
                                     @"COUPON TYPE: T  (A/C GROUP)",
                                     @"SORT SEQUENCE   :",
                                     @"====================================================================================================================================",
                                     @"AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION",
                                     @"REPORT ENTRIES FOR TECHLOG COUPONS(TYPE T)", nil];
            NSString *newTechLogEntryString = [NSString stringWithFormat:@"%@", @" ------------------------------------------------------------------------------------------------------------------------------------"];
            
            NSInteger maxLineCount = contentAsLines.count;
            BOOL newTechLogEntry = YES;
            
            for (int lineIndex = 0 ; lineIndex < maxLineCount ; ++lineIndex)
            {
                NSString *currentLine = (NSString*) contentAsLines[lineIndex];
                BOOL parseCurrentLine = YES;
                
                // Remove all lines from the file that are not required (META DATA LINES)
                for (NSString *excludedLine in excludedLines)
                {
                    if([currentLine rangeOfString:excludedLine].location != NSNotFound)
                    {
                        parseCurrentLine = NO;
                        break;
                    }
                }
                
                // Process the non META DATA LINE (ie the lines with the tech log entry data.
                if (parseCurrentLine && currentLine.length > 0)
                {
                    if (newTechLogEntry)
                    {
                        if (currentTechLogEntry != nil)
                        {
                            [techLogEntries addObject:currentTechLogEntry];
                        }
                        
                        // Reset the current tech log entry
                        currentTechLogEntry = [[NSMutableArray alloc] init];
                        newTechLogEntry = NO;
                    }
                    
                    if([currentLine rangeOfString:newTechLogEntryString].location != NSNotFound)
                    {
                        newTechLogEntry = YES;
                    }
                    
                    [currentTechLogEntry addObject:currentLine];
                }
            }
            
            // Process each tech log entry
            
            //hud.mode = MBProgressHUDModeDeterminate;
            hud.mode = MBProgressHUDModeAnnularDeterminate;
            hud.labelText = @"Progress";
            
            float hudInterval = 1.0f / techLogEntries.count;
            float hudProgress = 0.0f;
            for (int intervalStep = 0 ; intervalStep < techLogEntries.count ; ++intervalStep)
            {
                // Get the current tech log entry
                NSArray *currentTechLogEntry = (NSArray*) techLogEntries[intervalStep];
                
                if(intervalStep == 50)
                {
                    NSLog(@"PROCESSING %d of %d", intervalStep + 1, techLogEntries.count);
                    NSLog(@"%@", currentTechLogEntry);
                }
                
                // Update the hud
                hudProgress += hudInterval;
                hud.progress = hudProgress;
                
                // Parse the current techlog entry
                if(![self parseTechLogEntryFromArray:currentTechLogEntry onHUD:hud])
                {
                    [self.unimportedTechLogEntries addObject:currentTechLogEntry];
                }
                
                usleep(14200);
                
            }
            
            // Clean up - Store the managed data.
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Saving";
            [FSManagedDataManager save];
            sleep(1.42);
            
            if (self.unimportedTechLogEntries.count == 0)
            {
                //Finish up. Give positive indication that the load was complete.
                hud.labelText = @"Complete";
                sleep(1.42);
            }
        }
        else
        {
            // The URL is either not presented or is not a file url.
            // No attempt was made to load the URL.
            hud.hidden = YES;
            dispatch_sync(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Tech Log Load" message:@"Unable to import selected file. It is not a correctly formatted TECH01 report." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];});
            
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally
    {
        [[NSFileManager defaultManager] removeItemAtURL:url error:nil];
    }
}

-(BOOL) parseTechLogEntryFromArray:(NSArray *)techLogEntryContent onHUD:(MBProgressHUD *)hud
{
    NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc] init];
    [dateTimeFormatter setDateFormat:@"ddMMMyy"];
    [dateTimeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    if (techLogEntryContent.count < 7)
    {
        return NO;
    }
    
    @try
    {
        // Get the lines as individual objects
        NSString *line0 = (NSString*)techLogEntryContent[0];
        NSString *line1 = (NSString*)techLogEntryContent[1];
        NSString *line2 = (NSString*)techLogEntryContent[2];
        NSString *line3 = (NSString*)techLogEntryContent[3];
        NSString *line4 = (NSString*)techLogEntryContent[4];
        NSString *line5 = (NSString*)techLogEntryContent[5];
        
        
        // Line 0 Rego
        NSString *rego = [self subStringOfString:line0 startIndex:1 range:8 withTrimmedResult:YES];
        hud.labelText = rego;
        
        // Line 0 Sequence
        NSString *sequence = [self subStringOfString:line0 startIndex:38 range:4 withTrimmedResult:YES];
        
        // Create the tech log coupon
        TechLogCoupon *techLogCoupon = [TechLogCoupon initTechLogCouponWithRego:rego andSequence:sequence];
        
        // Get the Tech log entry from the core data store.
        TechLogEntry *techLogEntry = [TechLogEntry initTechLogEntryWithCoupon:techLogCoupon];
        
            
        // Line 0 Applied Date
        NSString *appliedDateString = [self subStringOfString:line0 startIndex:10 range:8 withTrimmedResult:YES];
        NSDate *appliedDate = [dateTimeFormatter dateFromString:appliedDateString];
        techLogEntry.appliedDate = appliedDate;
        //NSLog(@"%@", [appliedDate debugDescription]);
        
        // Line 0 Departure port
        NSString *departurePort = [self subStringOfString:line0 startIndex:19 range:3 withTrimmedResult:YES];
        techLogEntry.departurePort = departurePort;
        
        // Line 0 Destination port
        NSString *destinationPort = [self subStringOfString:line0 startIndex:23 range:3 withTrimmedResult:YES];
        techLogEntry.destinationPort = destinationPort;

        // Line 0 Ata sub
        NSString *ataSub = [self subStringOfString:line0 startIndex:28 range:8 withTrimmedResult:YES];
        techLogEntry.ataSub = ataSub;
        
        // Line 0 MEL
        NSString *mel = [self subStringOfString:line0 startIndex:44 range:16 withTrimmedResult:YES];
        techLogEntry.mel = mel;
        
        // Line 1 Category
        NSString *category = [self subStringOfString:line1 startIndex:0 range:61 withTrimmedResult:YES];
        techLogEntry.category = category;
        
        // Line 2 Tech Log Rep station
        NSString *techLogRepStation = [self subStringOfString:line2 startIndex:26 range:3 withTrimmedResult:YES];
        techLogEntry.techLogRepStation = techLogRepStation;
        
        // Line 2 Tech Log Rep date
        NSString *techLogRepDateString = [self subStringOfString:line2 startIndex:30 range:7 withTrimmedResult:YES];
        NSDate *techLogRepDate = [dateTimeFormatter dateFromString:techLogRepDateString];
        techLogEntry.techLogRepDate = techLogRepDate;
        //NSLog(@"%@", techLogRepDate);
        
        // Line 2 MEL due date
        NSString *dueDateString = [self subStringOfString:line2 startIndex:47 range:7 withTrimmedResult:YES];
        NSDate *dueDate = [dateTimeFormatter dateFromString:dueDateString];
        techLogEntry.dueDate = dueDate;
        //NSLog(@"%@", melDueDate);
        
        // Line 3 Tech Log Rep station
        NSString *mxiCameoRepStation = [self subStringOfString:line3 startIndex:26 range:3 withTrimmedResult:YES];
        techLogEntry.mxiCameoRepStation = mxiCameoRepStation;
        
        // Line 3 Tech Log Rep date
        NSString *mxiCameoRepDateString = [self subStringOfString:line3 startIndex:30 range:7 withTrimmedResult:YES];
        NSDate *mxiCameoRepDate = [dateTimeFormatter dateFromString:mxiCameoRepDateString];
        techLogEntry.mxiCameoRepDate = mxiCameoRepDate;
        //NSLog(@"%@", mxiCameoRepDate);
        
        // Line 3 wpStatus
        NSString *wpStatus = [self subStringOfString:line3 startIndex:38 range:22 withTrimmedResult:YES];
        techLogEntry.wpStatus = wpStatus;
        //NSLog(@"%@", wpStatus);
        
        // Line 4 Limitations
        NSString *limitations = [self subStringOfString:line4 startIndex:25 range:35 withTrimmedResult:YES];
        techLogEntry.limitationInfo = limitations;
        techLogEntry.performanceImpact = [NSNumber numberWithBool:[self isPerformanceImpactInLimitationsString:limitations]];
        techLogEntry.maintenanceImpact = [NSNumber numberWithBool:[self isMaintenanceImpactInLimitationsString:limitations]];
        techLogEntry.operationalImpact = [NSNumber numberWithBool:[self isOperationsImpactInLimitationsString:limitations]];
        //NSLog(@"%@", limitations);
        
        // Line 5 Techlog coupon
        NSString *couponNumber = [self subStringOfString:line5 startIndex:0 range:28 withTrimmedResult:YES];
        techLogEntry.coupon = couponNumber;
        
        
        // Read the report
        NSMutableString *report = [[NSMutableString alloc]initWithString:@""];
        NSString *extractedReportSegment = [self subStringOfString:line0 startIndex:62 range:100 withTrimmedResult:NO];
        if (extractedReportSegment != nil){[report appendString:extractedReportSegment];}
    
        extractedReportSegment = [self subStringOfString:line1 startIndex:62 range:100 withTrimmedResult:NO];
        if (extractedReportSegment != nil){[report appendString:extractedReportSegment];}
        
        extractedReportSegment = [self subStringOfString:line2 startIndex:58 range:104 withTrimmedResult:NO];
        if (extractedReportSegment != nil){[report appendString:extractedReportSegment];}
        
        extractedReportSegment = [self subStringOfString:line3 startIndex:54 range:108 withTrimmedResult:NO];
        if (extractedReportSegment != nil){[report appendString:extractedReportSegment];}
        techLogEntry.report = report;
        //NSLog(@"%@", report);
        
        // Read the action
        NSMutableString *action = [[NSMutableString alloc]initWithString:@""];
        NSString *extractedActionSegment = [self subStringOfString:line4 startIndex:62 range:100 withTrimmedResult:NO];
        if (extractedActionSegment != nil)
        {
            [action appendString:extractedActionSegment];
        }
        
        extractedActionSegment = [self subStringOfString:line5 startIndex:62 range:100 withTrimmedResult:NO];
        if (extractedActionSegment != nil)
        {
            [action appendString:extractedActionSegment];
        }

        
        NSInteger currentActionLineIndex = 6;
        BOOL continueProcessingActionLines = YES;
        NSString *logisticsStartLine = @"** LOGISTICS  REF/PART NO";
        NSString *lastLineOfTechLog = @" ----------";
        while(continueProcessingActionLines)
        {
            if (techLogEntryContent.count > currentActionLineIndex)
            {
                NSString *currentActionLine = (NSString*)techLogEntryContent[currentActionLineIndex];
                NSRange rangeLogisticsLine = [currentActionLine rangeOfString:logisticsStartLine];
                NSRange rangeLastLine = [currentActionLine rangeOfString:lastLineOfTechLog];
                if(rangeLogisticsLine.location != 38 && rangeLastLine.location != 0)
                {
                    extractedActionSegment = [self subStringOfString:currentActionLine startIndex:62 range:100 withTrimmedResult:NO];
                    if (extractedActionSegment != nil)
                    {
                        NSString *trimmedString = [extractedActionSegment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        if (trimmedString.length > 0)
                        {
                            [action appendString:extractedActionSegment];
                            ++currentActionLineIndex;
                        }
                        else
                        {
                            continueProcessingActionLines = NO;
                        }
                    }
                    else
                    {
                        continueProcessingActionLines = NO;
                    }
                }
                else
                {
                    continueProcessingActionLines = NO;
                }
            }
        }
        techLogEntry.action = action;
    
        // Check for a logistics line
        NSInteger currentLogisticsLineIndex = currentActionLineIndex;
        if (techLogEntryContent.count > currentActionLineIndex)
        {
            NSString *currentLogisticsLine = (NSString*)techLogEntryContent[currentLogisticsLineIndex];
            NSRange rangeLogisticsLine = [currentLogisticsLine rangeOfString:logisticsStartLine];
            if (rangeLogisticsLine.location == 38)
            {
                BOOL processingPartLines = YES;
                while(processingPartLines)
                {
                    currentLogisticsLineIndex++;
                    if (techLogEntryContent.count > currentActionLineIndex)
                    {
                    
                        currentLogisticsLine = (NSString*)techLogEntryContent[currentLogisticsLineIndex];
                        NSRange rangePart = [currentLogisticsLine rangeOfString:@"PART"];
                        if (rangePart.location == 52)
                        {
                            TechLogPart *techLogPart = [TechLogPart initTechLogEntryWithTechLogEntry:techLogEntry];
                            
                            NSString *partNumber = [self subStringOfString:currentLogisticsLine startIndex:57 range:26 withTrimmedResult:YES];
                            techLogPart.number = partNumber;
                            
                            NSString *partCategory = [self subStringOfString:currentLogisticsLine startIndex:84 range:3 withTrimmedResult:YES];
                            techLogPart.category = partCategory;
                            
                            NSString *partQuantity = [self subStringOfString:currentLogisticsLine startIndex:89 range:3 withTrimmedResult:YES];
                            techLogPart.quantity = partQuantity;
                            
                            NSString *partDemInfo = [self subStringOfString:currentLogisticsLine startIndex:94 range:20 withTrimmedResult:YES];
                            techLogPart.demInfo = partDemInfo;
                            
                            NSString *partIss = [self subStringOfString:currentLogisticsLine startIndex:115 range:3 withTrimmedResult:YES];
                            techLogPart.iss = partIss;
                            
                            NSString *partSupp = [self subStringOfString:currentLogisticsLine startIndex:120 range:4 withTrimmedResult:YES];
                            techLogPart.supp = partSupp;
                            
                            NSString *partLocDateString = [self subStringOfString:currentLogisticsLine startIndex:126 range:7 withTrimmedResult:YES];
                            if(partLocDateString.length == 6)
                            {
                                partLocDateString = [NSString stringWithFormat:@"0%@", partLocDateString];
                            }
                            NSDate *partLocDate = [dateTimeFormatter dateFromString:partLocDateString];
                            techLogPart.locDate = partLocDate;
                        }
                        else
                        {
                            processingPartLines = NO;
                        }
                    }
                }
            }
        }
        
        // Check for additional lines
        NSInteger currentAdditionalLineIndex = currentLogisticsLineIndex;
        NSMutableArray *additionalLines = [[NSMutableArray alloc] init];
        BOOL processingAdditionalLines = YES;
        while (processingAdditionalLines)
        {
            NSString *currentAdditionalLine = (NSString*)techLogEntryContent[currentAdditionalLineIndex++];
            NSRange rangeLastLine = [currentAdditionalLine rangeOfString:lastLineOfTechLog];
            if(rangeLastLine.location != 0)
            {
                [additionalLines addObject:[currentAdditionalLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            }
            else
            {
                processingAdditionalLines = NO;
            }
        }
        
        NSMutableString *additionalInformation = [[NSMutableString alloc] initWithString:@""];
        for (NSString* addInfo in additionalLines)
        {
            [additionalInformation appendFormat:@" %@", addInfo];
        }
        techLogEntry.additionalInformation = [additionalInformation stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"%@", techLogEntry.additionalInformation);
        
        //NSLog(@"%@", [additionalLines debugDescription]);
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        return NO;
        
    }
    @finally
    {
        return YES;
    }
}

#pragma mark Parse Utilities

-(NSString *) subStringOfString:(NSString*)sourceString startIndex:(NSInteger)startIndex range:(NSInteger)range withTrimmedResult:(BOOL)trimResult
{
    
    if (sourceString == nil || startIndex < 0 || range <= 0)
    {
        return nil;
    }
    
    
    @try
    {
        NSInteger sourceLength = sourceString.length;
        NSInteger maxIndexOfRange = startIndex + range - 1;
        if (startIndex >= sourceLength)
        {
            return nil;
        }
        else if (maxIndexOfRange < sourceLength)
        {
            NSString *subString = [sourceString substringWithRange:NSMakeRange(startIndex, range)];
            if (trimResult)
            {
                return [subString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
            else
            {
                return subString;
            }
        }
        else
        {
            NSString *subString = [sourceString substringFromIndex:startIndex];
            if (trimResult)
            {
                return [subString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            }
            else
            {
                return subString;
            }
        }

        
    }
    @catch (NSException *exception) {
        
        return nil;
    }
    @finally {
        
    }
}

-(BOOL) isPerformanceImpactInLimitationsString:(NSString*)limitationsString
{
    if(limitationsString == nil) {return NO; }
    int length = [limitationsString length];
    bool open = false;
    
    unichar limitType = 0;
    for(int i = 0 ; i < length ; ++i)
    {
        unichar c = [limitationsString characterAtIndex:i];
        if (c != ' ')
        {
            if (c == '{' || c == '(' || c == '[')
            {
                open = true;
            }
            else if (c == 'P' || c == 'O' || c == 'M')
            {
                limitType = c;
            }
            else if (c == '}' || c == ')' || c == ']')
            {
                if (open && limitType != 0)
                {
                    if (limitType == 'P')
                    {
                        return YES;
                    }
                }
                
                // Reset control vars
                open = NO;
                limitType = 0;
            }
        }
    }
    return NO;
}

-(BOOL) isOperationsImpactInLimitationsString:(NSString*)limitationsString
{
    if(limitationsString == nil) {return NO; }
    int length = [limitationsString length];
    bool open = false;
    
    unichar limitType = 0;
    for(int i = 0 ; i < length ; ++i)
    {
        unichar c = [limitationsString characterAtIndex:i];
        if (c != ' ')
        {
            if (c == '{' || c == '(' || c == '[')
            {
                open = true;
            }
            else if (c == 'P' || c == 'O' || c == 'M')
            {
                limitType = c;
            }
            else if (c == '}' || c == ')' || c == ']')
            {
                if (open && limitType != 0)
                {
                    if (limitType == 'O')
                    {
                        return YES;
                    }
                }
                
                // Reset control vars
                open = NO;
                limitType = 0;
            }
        }
    }
    return NO;
}

-(BOOL) isMaintenanceImpactInLimitationsString:(NSString*)limitationsString
{
    if(limitationsString == nil) {return NO; }
    int length = [limitationsString length];
    bool open = false;
    
    unichar limitType = 0;
    for(int i = 0 ; i < length ; ++i)
    {
        unichar c = [limitationsString characterAtIndex:i];
        if (c != ' ')
        {
            if (c == '{' || c == '(' || c == '[')
            {
                open = true;
            }
            else if (c == 'P' || c == 'O' || c == 'M')
            {
                limitType = c;
            }
            else if (c == '}' || c == ')' || c == ']')
            {
                if (open && limitType != 0)
                {
                    if (limitType == 'M')
                    {
                        return YES;
                    }
                }
                
                // Reset control vars
                open = NO;
                limitType = 0;
            }
        }
    }
    return NO;
}


#pragma mark Development Methods
-(NSURL *) testFileToLoad
{
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *bundlePath = [appBundle pathForResource:@"TECH01" ofType:@"txt"];
    NSURL *fileURL = [NSURL fileURLWithPath:bundlePath];
    //NSLog(@"%@", [fileURL absoluteString]);
    
    return fileURL;
}


@end
