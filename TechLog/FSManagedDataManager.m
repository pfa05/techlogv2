//
//  FSManagedDataManager.m
//  TechLog
//
//  Created by Peter Facey on 20/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "FSManagedDataManager.h"

@implementation FSManagedDataManager
static FSManagedDataManager *_manager;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


#pragma mark Singleton Methods
+(FSManagedDataManager *) manager
{
    if (_manager == nil)
    {
        _manager = [[FSManagedDataManager alloc] init];
    }
    return _manager;
}

+(void) save
{
    FSManagedDataManager *manager = [FSManagedDataManager manager];
    [manager saveContext];
}

-(NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil){
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FSTechLogModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

-(NSPersistentStoreCoordinator *) persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentDirectory] URLByAppendingPathComponent:@"FSTechLog.sqlite"];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        NSLog (@"Unresolved Error %@, %@", error, [error userInfo]);
        //abort();
    }
    return _persistentStoreCoordinator;
}

-(NSManagedObjectContext *) managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *storeCoordinator = [self persistentStoreCoordinator];
    if (storeCoordinator != nil)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:storeCoordinator];
    }
    
    return  _managedObjectContext;
}


#pragma mark Core Data Methods
-(NSURL *) applicationDocumentDirectory
{
    NSURL *applicationDirectoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    //NSLog(@"Application Directory : \"%@\"", [applicationDirectoryURL debugDescription]);
    return applicationDirectoryURL;
}

-(void) saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContect = [self managedObjectContext];
    if (managedObjectContect != nil)
    {
        if ([managedObjectContect hasChanges] && ![managedObjectContect save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


@end
