//
//  TechLogPart+Customisation.h
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogPart.h"
@class TechLogEntry;

@interface TechLogPart (Customisation)
+(TechLogPart*) initTechLogEntryWithTechLogEntry:(TechLogEntry*)entry;
@end
