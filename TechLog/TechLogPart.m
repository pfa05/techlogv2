//
//  TechLogPart.m
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogPart.h"
#import "TechLogEntry.h"


@implementation TechLogPart

@dynamic number;
@dynamic category;
@dynamic quantity;
@dynamic demInfo;
@dynamic iss;
@dynamic supp;
@dynamic locDate;
@dynamic techLogEntry;

@end
