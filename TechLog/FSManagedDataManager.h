//
//  FSManagedDataManager.h
//  TechLog
//
//  Created by Peter Facey on 20/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface FSManagedDataManager : NSObject
{
    
}
#pragma mark Singleton Methods
+(FSManagedDataManager *) manager;
+(void) save;

#pragma mark Properties
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


#pragma mark Core Data Methods
-(NSURL *) applicationDocumentDirectory;
-(void) saveContext;


@end
