//
//  FSSettingViewController.h
//  TechLog
//
//  Created by Peter Facey on 23/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FSSettingViewController;

@protocol FSSettingViewControllerDelegate <NSObject>

-(void) settingViewControllerDidComplete:(FSSettingViewController*)viewController;

@end
@interface FSSettingViewController : UITableViewController <FSSettingViewControllerDelegate>
@property (nonatomic, weak) id<FSSettingViewControllerDelegate> delegate;
- (IBAction)done:(id)sender;
@end
