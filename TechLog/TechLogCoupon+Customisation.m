//
//  TechLogCoupon+TechLogCouponCustomisation.m
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogCoupon+Customisation.h"
#import "FSManagedDataManager.h"

@implementation TechLogCoupon (Customisation)
+(TechLogCoupon *)initTechLogCouponWithRego:(NSString *)rego andSequence:(NSString *)seq
{
    FSManagedDataManager *dataManager = [FSManagedDataManager manager];
    NSManagedObjectContext *context = dataManager.managedObjectContext;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(rego == %@ AND seq == %@)", rego, seq];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"TechLogCoupon" inManagedObjectContext:context]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] > 0)
    {
        return (TechLogCoupon *) [results objectAtIndex:0];
    }
    
    TechLogCoupon *techLogCoupon = [NSEntityDescription insertNewObjectForEntityForName:@"TechLogCoupon" inManagedObjectContext:context];
    techLogCoupon.rego = rego;
    techLogCoupon.seq = seq;
    return techLogCoupon;
}
@end
