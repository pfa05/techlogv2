//
//  FSDiagnosisAssistant.h
//  TechLog
//
//  Created by Peter Facey on 23/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface FSDiagnosisAssistant : NSObject <UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
@property(nonatomic, strong) NSArray* unimportedTechLogEntries;
+(FSDiagnosisAssistant *) assistant;


@end
