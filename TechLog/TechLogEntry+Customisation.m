//
//  TechLogEntry+Customisation.m
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogEntry+Customisation.h"
#import "FSManagedDataManager.h"
#import "TechLogCoupon.h"

@implementation TechLogEntry (Customisation)
+(TechLogEntry*) initTechLogEntryWithCoupon:(TechLogCoupon *)coupon
{
    FSManagedDataManager *dataManager = [FSManagedDataManager manager];
    NSManagedObjectContext *context = dataManager.managedObjectContext;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(techLogCoupon == %@)", coupon];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"TechLogEntry" inManagedObjectContext:context]];
    [request setPredicate:predicate];
        
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] > 0)
    {
        return (TechLogEntry *) [results objectAtIndex:0];
    }
        
    TechLogEntry *techLogEntry = [NSEntityDescription insertNewObjectForEntityForName:@"TechLogEntry" inManagedObjectContext:context];
    techLogEntry.techLogCoupon = coupon;
    return techLogEntry;
}
@end
