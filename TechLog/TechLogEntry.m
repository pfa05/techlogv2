//
//  TechLogEntry.m
//  TechLog
//
//  Created by Peter Facey on 29/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogEntry.h"
#import "TechLogCoupon.h"
#import "TechLogPart.h"


@implementation TechLogEntry

@dynamic action;
@dynamic additionalInformation;
@dynamic appliedDate;
@dynamic ataSub;
@dynamic category;
@dynamic departurePort;
@dynamic destinationPort;
@dynamic dueDate;
@dynamic limitationInfo;
@dynamic maintenanceImpact;
@dynamic mel;
@dynamic mxiCameoRepDate;
@dynamic mxiCameoRepStation;
@dynamic operationalImpact;
@dynamic performanceImpact;
@dynamic report;
@dynamic coupon;
@dynamic techLogRepDate;
@dynamic techLogRepStation;
@dynamic wpStatus;
@dynamic techLogCoupon;
@dynamic techLogPart;

@end
