//
//  FSTech01Parser.h
//  TechLog
//
//  Created by Peter Facey on 18/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MBProgressHUD;

@interface FSTech01Parser : NSObject
#pragma mark Singleton Methods
+(FSTech01Parser *) parser;

@property(nonatomic, strong) NSMutableArray* unimportedTechLogEntries;
#pragma mark Import Methods
-(void) importFromURL:(NSURL*)url;
-(void) importFromURL:(NSURL*)url onView:(UIView *) view;
-(void) importFromURL:(NSURL*)url onHUD:(MBProgressHUD*)hud;
-(BOOL) parseTechLogEntryFromArray:(NSArray*)techLogEntryContent onHUD:(MBProgressHUD*)hud;

#pragma mark Parse Utilities
-(NSString *) subStringOfString:(NSString*)sourceString startIndex:(NSInteger)startIndex range:(NSInteger)range withTrimmedResult:(BOOL)trimResult;
-(BOOL) isPerformanceImpactInLimitationsString:(NSString*)limitationsString;
-(BOOL) isOperationsImpactInLimitationsString:(NSString*)limitationsString;
-(BOOL) isMaintenanceImpactInLimitationsString:(NSString*)limitationsString;


#pragma mark Development Methods
-(NSURL *) testFileToLoad;
@end
