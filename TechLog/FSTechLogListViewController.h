//
//  FSTechLogListViewController.h
//  TechLog
//
//  Created by Peter Facey on 23/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSSettingViewController.h"
@interface FSTechLogListViewController : UITableViewController <FSSettingViewControllerDelegate>

@end
