//
//  TechLogCoupon.m
//  TechLog
//
//  Created by Peter Facey on 29/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogCoupon.h"
#import "TechLogEntry.h"


@implementation TechLogCoupon

@dynamic seq;
@dynamic rego;
@dynamic techLogEntry;

@end
