//
//  FSDiagnosisAssistant.m
//  TechLog
//
//  Created by Peter Facey on 23/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "FSDiagnosisAssistant.h"



@implementation FSDiagnosisAssistant

static FSDiagnosisAssistant* _assistant;

+(FSDiagnosisAssistant*) assistant
{
    if (_assistant == nil)
    {
        _assistant = [[FSDiagnosisAssistant alloc] init];
    }
    return _assistant;
}


-(void) sendUnimportedEmailToDeveloper
{
    if (self.unimportedTechLogEntries.count > 0)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"au_AU"];
            [dateFormatter setLocale:locale];
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            NSString *subject = [NSString stringWithFormat:@"1TECH01 IMPORT FAILURE : %@", [dateFormatter stringFromDate:[NSDate date]]];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"pfacey@qantas.com.au",  nil];
            NSArray *ccRecipients = [NSArray arrayWithObjects:nil];
            NSArray *bccRecipients = [NSArray arrayWithObjects:nil];
            
            [mailer setSubject:subject];
            [mailer setToRecipients:toRecipients];
            [mailer setCcRecipients:ccRecipients];
            [mailer setBccRecipients:bccRecipients];
            
            NSMutableString *bodyText = [[NSMutableString alloc] init];
            [bodyText appendString:@"All,\n"];
            [bodyText appendFormat:@"BODY"];
            
            NSURL *reportURL = [self writeUnimportedTechLogEntries];
            NSString *path = [reportURL lastPathComponent];
            NSData *data = [[NSFileManager defaultManager] contentsAtPath:[reportURL path]];
            [mailer addAttachmentData:data mimeType:@"text/plain" fileName:path];
            
            [mailer setMessageBody:bodyText isHTML:NO];
            UIWindow *window = [UIApplication sharedApplication].keyWindow;
            UIViewController *rootViewController = window.rootViewController;
            [rootViewController presentModalViewController:mailer animated:YES];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
    }
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) //send email
    {
        [self sendUnimportedEmailToDeveloper];
    }
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *rootViewController = window.rootViewController;
    
    [rootViewController dismissModalViewControllerAnimated:YES];
    
}

-(NSURL *) applicationDocumentDirectory
{
    NSURL *applicationDirectoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    //NSLog(@"Application Directory : \"%@\"", [applicationDirectoryURL debugDescription]);
    return applicationDirectoryURL;
}

-(NSURL*) writeUnimportedTechLogEntries
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *reportURL = [NSURL URLWithString:@"IMPORT_ISSUES.TXT" relativeToURL:[self applicationDocumentDirectory]];
    
    
    NSMutableString *fileContent = [[NSMutableString alloc] init];
    [fileContent appendFormat:@"TECH01   ---  IMPORT ISSUES\n"];
    
    for (NSArray *currentTechLogEntry in self.unimportedTechLogEntries)
    {
        [fileContent appendFormat:@"\n"];
        for (NSString *currentTechLogEntryLine in currentTechLogEntry)
        {
            [fileContent appendFormat:@"\n%@", currentTechLogEntryLine];
        }
    }
    
    // If the file already exists, delete it.
    if ([reportURL checkResourceIsReachableAndReturnError:nil] == YES)
    {
        [fileManager removeItemAtPath:[reportURL path] error:nil];
    }
    
    [fileContent writeToURL:reportURL atomically:YES];
    
    return reportURL;
}



@end
