//
//  TechLogPart.h
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TechLogEntry;

@interface TechLogPart : NSManagedObject

@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * quantity;
@property (nonatomic, retain) NSString * demInfo;
@property (nonatomic, retain) NSString * iss;
@property (nonatomic, retain) NSString * supp;
@property (nonatomic, retain) NSDate * locDate;
@property (nonatomic, retain) TechLogEntry *techLogEntry;

@end
