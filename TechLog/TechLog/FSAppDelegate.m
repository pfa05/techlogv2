//
//  FSAppDelegate.m
//  TechLog
//
//  Created by Peter Facey on 18/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "FSAppDelegate.h"
#import "FSTech01Parser.h"

@implementation FSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialise the tech log importer singleton.
    
    NSURL *url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
    if (url != nil)
    {
        // return YES if the app does need to call handleOpenURL
        return YES;
    }
    else
    {
        // return NO if the app does not need to call handleOpenURL
        return NO;
    }
}


-(BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    FSTech01Parser* techLogParser = [FSTech01Parser parser];
    [techLogParser importFromURL:url];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*if (self.techLogUrlToLoadOnLaunch != nil)
    {
        NSURL *copyURL = self.techLogUrlToLoadOnLaunch;
        self.techLogUrlToLoadOnLaunch = nil;
        FSTech01Parser* techLogParser = [FSTech01Parser parser];
        [techLogParser importFromURL:copyURL];
    }*/
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
