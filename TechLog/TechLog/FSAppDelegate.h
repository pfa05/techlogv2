//
//  FSAppDelegate.h
//  TechLog
//
//  Created by Peter Facey on 18/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end
