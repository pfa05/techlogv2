//
//  FSAppParam.m
//  TechLog
//
//  Created by Peter Facey on 19/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "FSAppParam.h"

@implementation FSAppParam
static FSAppParam *_appParam;

#pragma Singleton Method
+(FSAppParam *) appParam
{
    if (_appParam == nil)
    {
        _appParam = [[FSAppParam alloc] init];
    }
    
    return _appParam;
}

#pragma mark UI methods
-(UIColor *) todayColor
{
    if(_todayColor != nil)
    {
        return _todayColor;
    }
    _todayColor = [UIColor colorWithRed:30/255.f green:119/255.f blue:225/255.f alpha:1.f];
    return _todayColor;
}

-(UIColor *) todayNavColor
{
    if(_todayNavColor != nil)
    {
        return _todayNavColor;
    }
    _todayNavColor = [UIColor colorWithRed:4/255.f green:24/255.f blue:51/255.f alpha:0.5f];
    return _todayNavColor;
}


-(UIColor *) yesterdayColor
{
    if(_yesterdayColor != nil)
    {
        return _yesterdayColor;
    }
    _yesterdayColor = [UIColor colorWithRed:252/255.f green:112/255.f blue:14/255.f alpha:1.f];
    return _yesterdayColor;
}

-(UIColor *) yesterdayNavColor
{
    if(_yesterdayNavColor != nil)
    {
        return _yesterdayNavColor;
    }
    _yesterdayNavColor = [UIColor colorWithRed:252/255.f green:112/255.f blue:14/255.f alpha:0.5f];
    return _yesterdayNavColor;
}

-(UIColor *) oldColor
{
    if(_oldColor != nil)
    {
        return _oldColor;
    }
    _oldColor = [UIColor colorWithRed:251/255.f green:72/255.f blue:79/255.f alpha:1.f];;
    return _oldColor;
}

-(UIColor *) oldNavColor
{
    if(_oldNavColor != nil)
    {
        return _oldNavColor;
    }
    _oldNavColor = [UIColor colorWithRed:251/255.f green:72/255.f blue:79/255.f alpha:0.5f];;
    return _oldNavColor;
}

-(UIColor *) defaultNavColor
{
    if(_defaultNavColor != nil)
    {
        return _defaultNavColor;
    }
    _defaultNavColor = [UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.5f];;
    return _defaultNavColor;
}


@end
