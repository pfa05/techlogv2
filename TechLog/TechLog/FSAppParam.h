//
//  FSAppParam.h
//  TechLog
//
//  Created by Peter Facey on 19/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//
//  Common storage facility for application specific param's

#import <Foundation/Foundation.h>

@interface FSAppParam : NSObject

#pragma Singleton Method
+(FSAppParam *) appParam;

#pragma mark UI methods
@property (strong, nonatomic) NSURL *importTechLogURL;
@property (strong, nonatomic) UIColor *todayColor;
@property (strong, nonatomic) UIColor *todayNavColor;
@property (strong, nonatomic) UIColor *yesterdayColor;
@property (strong, nonatomic) UIColor *yesterdayNavColor;
@property (strong, nonatomic) UIColor *oldColor;
@property (strong, nonatomic) UIColor *oldNavColor;
@property (strong, nonatomic) UIColor *defaultNavColor;

@end
