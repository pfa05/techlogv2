//
//  TechLogEntry.h
//  TechLog
//
//  Created by Peter Facey on 29/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TechLogCoupon, TechLogPart;

@interface TechLogEntry : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSString * additionalInformation;
@property (nonatomic, retain) NSDate * appliedDate;
@property (nonatomic, retain) NSString * ataSub;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * departurePort;
@property (nonatomic, retain) NSString * destinationPort;
@property (nonatomic, retain) NSDate * dueDate;
@property (nonatomic, retain) NSString * limitationInfo;
@property (nonatomic, retain) NSNumber * maintenanceImpact;
@property (nonatomic, retain) NSString * mel;
@property (nonatomic, retain) NSDate * mxiCameoRepDate;
@property (nonatomic, retain) NSString * mxiCameoRepStation;
@property (nonatomic, retain) NSNumber * operationalImpact;
@property (nonatomic, retain) NSNumber * performanceImpact;
@property (nonatomic, retain) NSString * report;
@property (nonatomic, retain) NSString * coupon;
@property (nonatomic, retain) NSDate * techLogRepDate;
@property (nonatomic, retain) NSString * techLogRepStation;
@property (nonatomic, retain) NSString * wpStatus;
@property (nonatomic, retain) TechLogCoupon *techLogCoupon;
@property (nonatomic, retain) NSSet *techLogPart;
@end

@interface TechLogEntry (CoreDataGeneratedAccessors)

- (void)addTechLogPartObject:(TechLogPart *)value;
- (void)removeTechLogPartObject:(TechLogPart *)value;
- (void)addTechLogPart:(NSSet *)values;
- (void)removeTechLogPart:(NSSet *)values;

@end
