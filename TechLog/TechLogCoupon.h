//
//  TechLogCoupon.h
//  TechLog
//
//  Created by Peter Facey on 29/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TechLogEntry;

@interface TechLogCoupon : NSManagedObject

@property (nonatomic, retain) NSString * seq;
@property (nonatomic, retain) NSString * rego;
@property (nonatomic, retain) NSSet *techLogEntry;
@end

@interface TechLogCoupon (CoreDataGeneratedAccessors)

- (void)addTechLogEntryObject:(TechLogEntry *)value;
- (void)removeTechLogEntryObject:(TechLogEntry *)value;
- (void)addTechLogEntry:(NSSet *)values;
- (void)removeTechLogEntry:(NSSet *)values;

@end
