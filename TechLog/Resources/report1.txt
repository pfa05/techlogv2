1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   1
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 COUPON TYPE: T  (A/C GROUP) 330380 MEL: O CDL: O
 SORT SEQUENCE   :   A/C REGN,DATE,SEQUENCE                                                                                         
 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

  REPORT ENTRIES FOR TECHLOG COUPONS(TYPE T)

 VH-EBC   20APR13  SIN-MEL  78-31     287   78-30-01        R-AT REVERSE SELECTION ON LANDING REV #1 SLOW TO REV GREEN.THEN AT REV O
                                                C             FF ECAM: REV #1 UNLOCKED, REV 1 FAULT.THRUST REVERSE RE DEPLOYED. REV 
    TECHLOG REP STN/DATE: SYD/23APR13          30APR13    1 FAULT RAMAINED AFTER STOWING.FAULT DISAPPEARED AFTER SHUTDOWN.
  CAMEO/MXI REP STN/DATE: SYD/23APR13 COMMIT
         LIMITATION/INFO:  [P]                              A-TEST C/OUT AS PER AMM 78-31-710-802. FAILED TO STOW WITH MSG RIGHT TR
         T00442S0                                             POSITION. ENG#1 TRUST REVERSER STOWED AND LOCKED OUT AS PER AMM 78-31-
                                                              00 PB 401 REV 01/10/2012 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART 3269086-32                  Y   001  STK@SDC
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBE   18APR13  MEL-BKK  27-22     480   27-22-01        R-AFTER SHUTDOWN F/CTL RUD TRIM 2 FAULT                                 
                                                C                                                                                   
    TECHLOG REP STN/DATE: BNE/21APR13          28APR13
  CAMEO/MXI REP STN/DATE: BNE/21APR13 IN WORK
         LIMITATION/INFO:  NIL                              A-279434 FCSC2(3CE2)/WRG:FCSC2 DGO5 BUS/DSO1 DISCRETE,SYSTEM TEST FAILED
         T0043KQW                                             .MAINTENANCE PROCEDURE CARRIED OUT,MEL APPLIED.TO HOLD MCA 1058.'O' PR
                                                              OCEDURE REQD TO BE CARRIED OUT BEFORE EACH FLIGHT 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBF   19APR13  KIX-SIN  25-60     111   25-65-01        R-REF CCL SEQ 201, PHYSICIAN KIT OPEN & USED.                           
                                                A                                                                                   
    TECHLOG REP STN/DATE: MEL/24APR13          *25APR13*
  CAMEO/MXI REP STN/DATE: MEL/24APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-MEL 25-65-01 (A) APPLIED. TO BE REPLACED AT NEXT AUSTRAILIAN PORT. MAI
         T0043S06                                             NT. WATCH SYSTEM DOWN, MCA# WILL BE LEFT BLANK PER MAINT. WATCH ADVICE
                                                              . 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART PHYSICIANKITLARGE           Y   001  STK@MIT
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBF   20APR13  SIN-AKL  23-61     118   23-01           R-REF SEQ 116 LIGHTNING STIKE, R/H HORIZONTAL STABILISER TRAILING EDGE O
                                       DEFECT CAT X NOT FOUND UTBD STATIC WICK HAS 'EXIT POINT' DAMAGE                              
    TECHLOG REP STN/DATE: MEL/24APR13
  CAMEO/MXI REP STN/DATE: MEL/24APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-DEFERRED FOR REPLACEMENT. OPS PER CDL 23-1 
         T00441V5
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART E0399-01                    Y   001  AVAIL SYD MEL
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   2
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

 VH-EBH   10APR13  PER-SYD  23-61     827   23-01           R-ENGINEERS WAI:  L HORIZONTAL STABILIZER TIP, AFT MOST STATIC DISCHARGE
                                       DEFECT CAT X NOT FOUND  WICK HAS INSULATION MATERIAL ADRIFT. NOTE: REQ'S P/N E0400-01 OR 7400
    TECHLOG REP STN/DATE: SYD/24APR13                     07
  CAMEO/MXI REP STN/DATE: SYD/24APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-STAB TIP STATIC DISCHARGER NOW SUCCESSFULLY REMOVED. PLS REFER JOB STE
         T0041886                                             PS
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART CR2249-4-02                 Y   004  XFR FROM BCS
                                                    PART NSA936222-02                Y   001  XFR FROM BCS
                                                    PART 740007                      Y   001  STK@MIT
   RA EMAILED
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBH   22APR13  SYD-PER  21-63     891   21-63-03A       R-HOT AIR1 VALVE FAILED CLOSED                                          
                                                C                                                                                   
    TECHLOG REP STN/DATE: SYD/24APR13          2MAY13
  CAMEO/MXI REP STN/DATE: SYD/24APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-ZC RESET RESTORED NOM FUNCT HOWEVER DUE EXTENSIVE HISTORY MEL21-63-03A
         T00448AA                                              APPLIED CLOSURE FUNCTION OF NO1 PACK FCV VERIFIED TO HOLD 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBO   22APR13  SYD-PER  33-41     479   33-40-04B       R-WHITE TAIL LIGHT INOP ON SYS 1.                                       
                                                C                                                                                   
    TECHLOG REP STN/DATE: SYD/23APR13          2MAY13
  CAMEO/MXI REP STN/DATE: SYD/23APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-MEL 33-40-04B APPLIED. SYSTEM 2 SELECTED. 
         T0044A1A
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART 8GH005597-12                Y   001  STK@SBS
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBQ   20APR13  HNL-MEL  33-41      27   33-41-01        R-LEFT HAND SYS 1 NAV LIGHT INOP                                        
                                                C                                                                                   
    TECHLOG REP STN/DATE: SIN/23APR13          30APR13
  CAMEO/MXI REP STN/DATE: SIN/23APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-MEL 33-41-01 A) CAT C APPLIED DUE NIL GROUND TIME TO RECTIFY. MCA EBQ
         T00443AU                                             1065 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART CM8A103                     Y   001
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-EBQ   22APR13  MEL-SIN  25-65      38   25-65-01        R-PHYSICAN KIT LARGE LABELED USED BY HAVING YELLOW SECURE LOCK.         
                                                A                                                                                   
    TECHLOG REP STN/DATE: MEL/23APR13          23APR13
  CAMEO/MXI REP STN/DATE: MEL/23APR13 ACTV
         LIMITATION/INFO:  NIL                              A-MEL 25-65-01(A) APPLIED. MCA NO: EBQ 1066. TO HOLD FOR REPLACEMENT AT
         T0044AB3                                             AUSTRALIAN PORT. 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART PHYSICIANKITLARGE           Y   001  STK@MIT
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   3
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

 VH-QPB   21APR13  SYD-SIN  25-33     234   25-35-51A       R-FROM CCL 091. AFT GALLEY COMPACTOR U/S. FOUND RAM WILL NOT COMPACT ALL
                                                C              THE WAY DOWN.                                                        
    TECHLOG REP STN/DATE: SYD/22APR13          1MAY13
  CAMEO/MXI REP STN/DATE: SYD/22APR13 COMMIT
         LIMITATION/INFO:  AFT COMP INOP                    A-MEL APPLIED/PLACARDED 
         T00443BC
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART 3210005WL104                Y   001
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-QPC   20APR13  PVG-SYD  32-53     264   32-53-02A       R-NOSE WHEEL OVERSTEER FAULT LIGHT ILLUMINATED BEFORE TOW.              
                                                C                                                                                   
  **TECHLOG REP STN/DATE: SYD/25APR13          30APR13
  CAMEO/MXI REP STN/DATE:
         LIMITATION/INFO:  NIL                              A-L/H OVERSTEER SWITCH FOUND U/S. P/N 8-552-01 DEM # D47212/8. NIL TIME
         T0043WRE                                             TO REPLACE. MEL 32-53-02A APPLIED. EA:SA09047 ISSUED TO REMOVE & CAP C
                                                              ONNECTOR FROM SWITCH. 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART 8-552-01                    Y   001  D47212 8              Y    131  20APR13
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-QPF   17APR13  CGK-SYD  25-27      78   25-27-50A       R-REF CCL# 860, FLOOR HEATER L4 NOT WORKING                             
                                                C                                                                                   
  **TECHLOG REP STN/DATE: SYD/23APR13          27APR13
  CAMEO/MXI REP STN/DATE: SYD/24APR13 ACTV
         LIMITATION/INFO:  NIL                              A-NIL TIME, MEL 25-27-50A APPLIED. FLOOR HEATER L4 DEACTIVATED IAW AMM 2
         T0043FJQ                                             5-27-00-040-801. C/B FIN 9DS TRIPPED NAD TAGGED 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART Z212H0004110                Y   001
                                                    PART 4E4640-3                    Y   001
                                                    PART 4E4641-1                    Y   001
   PLAN BY DATE SET TO 23APR
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-QPG   20APR13  PER-SIN  23-72     480   23-72-01A       R-FLT DECK SECURITY CAMERA U/S.                                         
                                                C                                                                                   
    TECHLOG REP STN/DATE: PER/24APR13          30APR13
  CAMEO/MXI REP STN/DATE: PER/24APR13 ACTV
         LIMITATION/INFO:  NIL                              A-COCKPIT VIDEO SUPPLY CB FIN 15RA RESET TO NO AVAIL. MEK APPLIED TO HOL
         T0043XTJ                                             D. 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART 8420B1-201                  Y   001  TO PER
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-QPH   22APR13  MEL-SIN  23-51     279   23-52-07A       R-ON CAPT'S AUDIO CONTROL PANEL, VHF 2 RECEPTION KNOB DOES NOT ILLUM WHE
                                                C             N SELECTED                                                            
    TECHLOG REP STN/DATE: MEL/23APR13          2MAY13
  CAMEO/MXI REP STN/DATE: MEL/23APR13 ACTV
         LIMITATION/INFO:  NIL                              A-RECETION OPS IS OK. JUST LIGHT U/S 
         T0044A40
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART ACP2788AE01                 Y   001  XFR TO MIT_D51090
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   4
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

 VH-QPI   18APR13  SYD-SIN  25-21     343   25-20-52A       R-W.R.T CCL 661, SEAT 2K ELECTRICAL INOPERATIVE.                        
                                                C                                                                                   
    TECHLOG REP STN/DATE: PER/23APR13          28APR13
  CAMEO/MXI REP STN/DATE: PER/23APR13 COMMIT
         LIMITATION/INFO:  2K NIL ELECT                     A-DEFECT CONFIRMED. SEAT ELECTRICAL RENDERED INOP. SEAT SECURED UPRIGHT
         T0043K5T                                             & CONTROLLER DEACTIVATED. MEL APPLIED TO HOLD. 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART Z295H8204300                Y   001  PER
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQA   26FEB13  SYD-LAX  53-55     202   53-28           R-REF T0026NSH- WING TO BODY FAIRING (UPPER) 195AT FILLET FAIRING FOUND 
                                       DEFECT CAT X NOT FOUND DAMAGE.                                                               
  **TECHLOG REP STN/DATE: MNL/5APR13
  CAMEO/MXI REP STN/DATE: MNL/8APR13  IN WORK
         LIMITATION/INFO:  NIL                              A-IAW EA: SS30911 SPEED TAPE APPLIED OVE SEAL AREA. CDL 53-28 APPLIED. R
         T0026PHE                                             EF IPC 53-35-13-02A ITEM 130A FOR P/N REQ. TO HOLD. 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART L5338192220400              Y   001  C23721                N          2MAR13
   REFER P/O S075064401 FROM AIRBUS SCHEDULED TO SHIP 10TH MAR
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQA   24MAR13  MNL-MNL  05-24     307   54-02           R-DURING GVI OF EXTERNAL SURFACE OF ENGINE NO.1 NACELL & PYLON, FOUND TH
                                       DEFECT CAT X NOT FOUND AT O/B FAN EXHAUST UPPER HINGE ACCESS COVER PANEL(418BR) DELAMINATED O
  **TECHLOG REP STN/DATE: MNL/5APR13                      N FWD EDGE STRIP.  ###NO.1 ENGINE TYPED ERROR, ACTUAL DEFECT ON NO.4 E
  CAMEO/MXI REP STN/DATE: MNL/8APR13  IN WORK         NGINE.
         LIMITATION/INFO:  NIL                              A-FURTHER INSPECTION FOUND APRON SEAL DISLODGE FROM PANEL. SEAL REMOVED
         T003X4E1                                             AND CDL 54-02 APPLIED. IAW SRM 54-32-07-283-802-A-01 TEMP REPAIR C/O T
                                                              O FWD OBD EDGE, ALUMINIUM TAPE APPLIED. TO HOLD FOR PERMANENT REPAIR W
                                                              ITHIN 100 FC. REPEAT INSPECTION TO BE C/O WITHIN 50 FC.
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART ASL4517-56-0                Y   001  C84421 4              N         31MAR13
   RPO-RS31034501 SCHD - REPAIR GROUP TO ADVISE
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQA    3APR13  DXB-SYD  21-59     347   21-59-01B       R-#1 SCS CONTROLLER & #1 SCS FAN ROBBED TO SERV VH-OQB                  
                                                C                                                                                   
  **TECHLOG REP STN/DATE: MNL/5APR13           *14APR13*
  CAMEO/MXI REP STN/DATE: MNL/8APR13  IN WORK
         LIMITATION/INFO:  NIL                              A-PARTS EX OQB FITTED TO OQA AND ATP 3352 ISSUED TO ALLOW MEL-51-01B TO
         T00406SD                                             BE APPLIED TO OQA AS PER ATP 3345 TIL 14 APR 13 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQB   20APR13  LAX-MEL  54-50     463   54-05           R-ON WAC NBR 3 ENG PYLON TO WING PANEL 473CL SEAL WORN/DAMAGED AT FWD CO
                                       DEFECT CAT X NOT FOUND RNER                                                                  
  **TECHLOG REP STN/DATE:
  CAMEO/MXI REP STN/DATE: LAX/24APR13 ACTV
         LIMITATION/INFO:  [P]CHK EACH FLT                  A-DAMAGE BEYOND AMM 54-53-11-200-801-A LIMITS.CDL 54-05 APPLIED.DAMAGE A
         T00442SL                                             REA COVERED WITH HIGH SPEED TAPE.TO BE INSPECTED BEFORE EACH FLT.15 CY
                                                              CLE LIMIT.MAX SPEED LIMIT.TO HOLD. 
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART L5451112120000              Y   001
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   5
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

 VH-OQC   21MAR13  SYD-HKG  78-32     335   78-01           R-ON WALKAROUND FOUND A DRAG LINK FAIRING MISSING AT 7 O'CLOCK ON #3 ENG
                                       DEFECT CAT X NOT FOUND                                                                       
    TECHLOG REP STN/DATE:
  CAMEO/MXI REP STN/DATE:
         LIMITATION/INFO:  NIL                              A-TO HOLD ITEM 
         T003WJET
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART ASL0622-00-6                Y   001  PO-S075246201
                                                    PART ASL0623-01-0                Y   001  STK SYD
                                                    PART HST11AP6-24                 Y   002  STK SYD
                                                    PART MS24665-302                 Y   001  STK SYD
                                                    PART NAS7603U3                   Y   001  STK SYD
                                                    PART VLC97DUWC6                  Y   002  STK SYD
   P/N ASL0622-00-6 AIRCELLE PO-S075246201 CLEARANCE SENT-31MAR13
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQC   21APR13  LAX-MEL  25-21     475   25-20-01A       R-J CLASS SEAT 11E NOT AVAILABLE FOR ELECTRIC OPS.                      
                                                C                                                                                   
    TECHLOG REP STN/DATE: LHR/23APR13          1MAY13
  CAMEO/MXI REP STN/DATE: LHR/23APR13 COMMIT
         LIMITATION/INFO:  11E NIL USE                      A-SEAT CAL C/O TO NO AVAIL. LEG REST RECLINE CABLE ALSO PREVIOUSLY U/S.
         T0044962                                             MEL 25-20-01A (M) CAT C APPLIES. 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQD   22MAR13  LAX-SYD  29-13     711   29-21-02A       R-DURING TOW TO DEPR GATE HYDRAULIC LEAK EVIDENT IN #3 PYLON AREA ###PLE
                                                C             ASE APPLY 10DAY EXTENSION TO EXPIRY DATES OF MEL 29-21-02A (ATP 3350) 
  **TECHLOG REP STN/DATE: SYD/20APR13          *24APR13*  AND EA:SM08014 IAW ATP3355 AND EA:SM08178 (ATTACHED TO THIS MX TASK)
  CAMEO/MXI REP STN/DATE: SYD/23APR13 COMMIT          ###PLEASE APPLY FURTHER ATP3360 AND EA SM08231 EXTENSION UNTIL NEXT 'A
                                                              ' CHECK (CURRENTLY SCHEDULED FOR 24APR13)
         LIMITATION/INFO:  NIL                              A-AS PER MOC INSTRUCTIONS, ATP NUMBER 3360 ISSUED TO EXTEND CAT "C" MEL
         T003WW10                                             29-21-02A UNTIL THE NEXT A CHECK (CURRENTLY SCHEDULED FOR 24APR13) IN
                                                              CONJUNCTION WITH EA:SM08231. INTENT OF EA:SM08231 C/OUT, NIL LEAKS EVI
                                                              DENT. TL SEQ 848 REFLECTS THIS INFORMATION.
   ATP 3355**   MEL CAT "D" MASTER *********
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQD   21APR13  LAX-SYD  33-20     847   33-20-05A       R-REF CABIN LOG SEQ 125. OVERHEAD LIGHTING ABOVE SEAT 88FG HAS A RED HUE
                                                C              WHEN LIGHTING IS PUT ON SLEEP CYCLE.                                 
    TECHLOG REP STN/DATE: SYD/23APR13          1MAY13
  CAMEO/MXI REP STN/DATE: SYD/23APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-DEFECT CONFIRMED. BALLAST P/NO. 1B5000-131100-4 OR -5 REQUIRED BUT NIL
         T00441XY                                              STOCK. MEL 33-20-05A APPLIED UNTIL SPARES AVAILABLE. 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQD   21APR13  LAX-SYD  21-59     849   21-59-02B       R-PRE DEPARTURE ECAM MESSAGE VENT COOLG SYS 2 OVHT DISPLAYED. ###ALSO RE
                                                C             FER TO MXI T00430M6                                                   
    TECHLOG REP STN/DATE: SYD/23APR13          1MAY13
  CAMEO/MXI REP STN/DATE: SYD/23APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-MEL 21-59-02B APPLIED. C/B'S FIN 998HW3, 999HW3, 2HW & 4HW PULLED & TA
         T00442CW                                             GGED. DISREGARD VENT COOL G SYS PROT FAULT OR COOLG SYS2 OVHT PROT ECA
                                                              MS IF DISPLAYED. (O)(M). 
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   6
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

 VH-OQE   16APR13  LAX-SYD  46-11     512   46-10-06A       R-MWR - ROB ANSU OPS #1 (FIN 5TL1) TO SERVICE VH-OQB ###REF ORIGIONAL AD
                                                C             HOC TASK (T004320Q) FOR REMOVAL OF ANSU OPS #1.                       
    TECHLOG REP STN/DATE: SYD/23APR13          *29APR13*
  CAMEO/MXI REP STN/DATE: SYD/23APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-NOTE: CONFIG CONTROL HAVE REMOVED ANSU OPS #1 FROM MX AS BEING A MANDA
         T0043Q5Z                                             TORY PART.<BR><BR>PLS REVERSE WHEN NEW PART FITTED.
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART 251375101-1102              Y   001  D42464 0              Y    H96  20APR13
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQF   18APR13  LAX-MEL  35-10     522   35-10-02A       R-L/H OXY MASK AT FIRST OBSERVER SEAT WILL NOT TEST                     
                                                C                                                                                   
    TECHLOG REP STN/DATE: MEL/22APR13          28APR13
  CAMEO/MXI REP STN/DATE: MEL/22APR13 COMMIT
         LIMITATION/INFO:  NO FIRST OBSERV                  A-DEFTECT CONFIRMED  MEL 35-10-02A CAT C 
         T0043HZ1
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART MF20-542                    Y   001  DEM T MELB D43761
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQF   18APR13  MEL-DXB  33-21     523   33-21-02A       R-MAIN CREW REST CEILING LIGHT U/S. READING LIGHTS OK.                  
                                                C                                                                                   
    TECHLOG REP STN/DATE: MEL/22APR13          28APR13
  CAMEO/MXI REP STN/DATE: MEL/22APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-RELAMPED AND RECONNECTED BALLAST CONNECTOR BUT CREW REST CEILING LIGHT
         T0043P9E                                              STILL U/S. SUSPECT BAD BALLAST (P/N 1A5000-280000-4). <BR>BALLAST NIL
                                                               STOCK IN LAX. MEL REMAINS
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQF   21APR13  MEL-LAX  25-24     538   25-24-50A       R-FROM CCL 464 - 16A/B OVERHEAD BIN U/S.                                
                                                C                                                                                   
    TECHLOG REP STN/DATE: MEL/22APR13          1MAY13
  CAMEO/MXI REP STN/DATE: MEL/22APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-TO HOLD IAW MEL 25-24-50A. BIN EMPTY AND SECURED CLOSED 
         T00447KX
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQG    5APR13  SYD-DXB  33-21     453   33-20-05A       R-RCL 512 SEAT 16F OVERHEAD STOWAGE BIN LIGHT REMAINS FULL BRT          
                                                C                                                                                   
  **TECHLOG REP STN/DATE: SYD/5MAY13           *10MAY13*
  CAMEO/MXI REP STN/DATE: SYD/5MAY13  ACTV
         LIMITATION/INFO:  NIL                              A-ACCOMPLISHED FURTHER WIRE CHECKS AND CONFIRMED LISTED FAULT. <BR>HOWEV
         T0040C4G                                             ER FINDING CONFLICTING INFO AS PER AWM POST MOD DRAWING 33-21-96 P 003
                                                              5 SHEET 1 AND AWM 33-21-21-96 P OO12 SHEET 2.<BR><BR>AS PER 33-21-96 P
                                                               0012 SHEET 2 WHEN CONNECTOR (C610CV648) DISCONNECTED STILL HAD STRAY
                                                              VOLTAGE AT BALLAST. (225LG45)<BR><BR>WILL REQUIRE FURTHER T/S.WHEN TIM
                                                              E AVAIL
                                      ** LOGISTICS  REF/PART NO                     CAT  QTY  DEM NO/INFO          ISS  SUPP LOC/DATE
                                                    PART Z035H0001110                Y   001
   DEM T SDC D16009
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   7
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

 VH-OQG   18APR13  SYD-LAX  78-26     499   78-01           R-FOUND #2 ENG THRUST REVERSER DRAG LINK FITTING FAIRING, AT 2 O'CLOCK P
                                       DEFECT CAT X NOT FOUND OSITION, MISSING                                                      
    TECHLOG REP STN/DATE:
  CAMEO/MXI REP STN/DATE:
         LIMITATION/INFO:  [P]                              A-CHECKED, THE FAIRING P/N ASL 0623-01-0 REQ AND SPARE AVAILABLE AT LAX.
         T0043Q0T                                              HOWEVER THE ATTACHMENT BRACKET P/N ASL 0622-00-6 REQ REPLACEMENT AND
                                                              THIS PART IS NOT AVAIL AT LAX. TO HOLD , CDL78-01 APPLIED 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQI   20APR13  MEL-DXB  54-53     197   54-05           R-RE: T/LOG 193, WING-TO-PYLON SEAL ON #3 ENG, ADJACENT PANEL 473AL, DET
                                       DEFECT CAT X NOT FOUND ERIORATED, CDL LIMITS TO APPLY ###PLEASE REFER HISTORY T/L 193 - LIMIT
    TECHLOG REP STN/DATE: SYD/22APR13                     ATION APPLIED IAW MAINT MEMO M13-1342 HOWEVER SPEED RESTRICTION AND TE
  CAMEO/MXI REP STN/DATE: SYD/22APR13 COMMIT          MP TAPE REPAIR INADVERTENTLY APPLIED IAW CDL.  ON INSPECTION DXB - SPE
                                                              ED TAPE FOUND AND CDL APPLIED DUE NIL TIME TO REMOVE TAPE AND REINSPEC
                                                              T SEAL.  PLEASE REMOVE SPEED TAPE AND RE-EVALUATE DETERIORATION IAW MA
                                                              INT MEMO M13-1342. IF STILL APPLICABLE APPLY LIMIT IAW MAINT MEMO FOR
                                                              REMAINING 3 FLIGHT SECTORS (TO REMOVE CDL SPEED RESTRICTION).
         LIMITATION/INFO:  [P]CHK EACH FLT                  A-SEAL RE-INSPECTED IAW AMM 54-53-11-200-801-A AND FOUND EROSION OUT OF
         T00440Z2                                             LIMITS. CDL 54-05 REMAINS. SPEED TAPE RE APPLIED. HOLD REMAINS FOR 15
                                                              FLT CYCLES AND SPEED RESTRICTIONS STILL APPLY
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQJ   20APR13  DXB-SYD  21-59     921   21-59-02B       R-ON DEPARTURE VENT COOL G SYS 2 OVERHEAT ECAM MSG APPEARED. ###ALSO REF
                                                C             ER TO MXI T003UMKQ                                                    
    TECHLOG REP STN/DATE: SYD/22APR13          30APR13
  CAMEO/MXI REP STN/DATE: SYD/22APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-MEL 21-59-02B APPLIED [O] [M]. C/B 998HW3, 999HW3, 2HW & 4HW PULLED AS
         T004423H                                              PER MEL. 
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQL   20APR13  SYD-DXB  34-55     848   34-50-03A       R-R VOR DISPLAYED RED ENTIRE FLIGHT                                     
                                                C                                                                                   
    TECHLOG REP STN/DATE: LAX/23APR13          30APR13
  CAMEO/MXI REP STN/DATE: LAX/23APR13 COMMIT
         LIMITATION/INFO:  NIL                              A-C/B VOR 2 CYCLED AND TSM 34-55-00-700-001-A C/O. BITE TEST C/O - TEST
         T00441CF                                             FAIL. FAULT CODE 3455F72Z SHOWN. FAULTS VOR 2 RECIEVER. HOLD REMAINS F
                                                              OR REPLACEMENT.
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQL   20APR13  SYD-DXB  27-24     850   27-24-01B       R-RUDDER ELECT ACTUATOR FAULT                                           
                                                C                                                                                   
    TECHLOG REP STN/DATE:                      30APR13
  CAMEO/MXI REP STN/DATE:
         LIMITATION/INFO:  NIL                              A-ACTION 2 CERTIFIED SATIS
         T00441DB
 ------------------------------------------------------------------------------------------------------------------------------------
 VH-OQL   22APR13  LHR-DXB  21-59     856   21-59-02B       R-VENT COOL G SYS 2 OVHT ON DEPARTURE                                   
                                                C                                                                                   
    TECHLOG REP STN/DATE:                      2MAY13
  CAMEO/MXI REP STN/DATE:
         LIMITATION/INFO:  NIL                              A-MEL APPLIED 21-59-02A SCS2 INOP C/BS TRIPPED 999HW3/998HW3 ,4HW/2HW. (
         T00449B0                                             M) PROCEDURE COMPLETED HOLD ITEM 
 ------------------------------------------------------------------------------------------------------------------------------------
1TECH01                                           Q A N T A S    A I R W A Y S    L T D.                 23 APR 13  06:25    PAGE   8
                                      TECHNICAL  LOG  DEFECTS  FOR  PERIOD   2DEC2005  TO  24APR2013

 ====================================================================================================================================
 AIRCRAFT  DATE    SECTOR   ATA-SUB   SEQ   MEL/DUE DATE                  REPORT  &  ACTION
 ====================================================================================================================================

-                                          * * *   END OF REPORT   * * *
