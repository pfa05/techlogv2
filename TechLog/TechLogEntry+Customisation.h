//
//  TechLogEntry+Customisation.h
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogEntry.h"
#import "TechLogCoupon.h"

@interface TechLogEntry (Customisation)
+(TechLogEntry*) initTechLogEntryWithCoupon:(TechLogCoupon*)coupon;
@end
