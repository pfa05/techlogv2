//
//  TechLogCoupon+TechLogCouponCustomisation.h
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogCoupon.h"

@interface TechLogCoupon (Customisation)
+(TechLogCoupon *)initTechLogCouponWithRego:(NSString*)rego andSequence:(NSString *)seq;

@end
