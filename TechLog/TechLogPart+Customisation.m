//
//  TechLogPart+Customisation.m
//  TechLog
//
//  Created by Peter Facey on 22/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "TechLogPart+Customisation.h"
#import "TechLogEntry.h"
#import "FSManagedDataManager.h"

@implementation TechLogPart (Customisation)
+(TechLogPart*) initTechLogEntryWithTechLogEntry:(TechLogEntry *)entry
{
    FSManagedDataManager *dataManager = [FSManagedDataManager manager];
    NSManagedObjectContext *context = dataManager.managedObjectContext;
    
    TechLogPart *techLogPart = [NSEntityDescription insertNewObjectForEntityForName:@"TechLogPart" inManagedObjectContext:context];
    techLogPart.techLogEntry = entry;
    return techLogPart;
}
@end
