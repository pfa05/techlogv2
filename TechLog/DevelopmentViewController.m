//
//  DevelopmentViewController.m
//  TechLog
//
//  Created by Peter Facey on 19/04/13.
//  Copyright (c) 2013 Flightscape. All rights reserved.
//

#import "DevelopmentViewController.h"
#import "FSTech01Parser.h"

@interface DevelopmentViewController ()

@end

@implementation DevelopmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Action
- (IBAction)testTech01Import:(id)sender
{
    FSTech01Parser *parser = [FSTech01Parser parser];
    NSURL *testTechLogURL = [parser testFileToLoad];
    [parser importFromURL:testTechLogURL];
    
}

- (IBAction)testReport2:(id)sender
{
    FSTech01Parser *parser = [FSTech01Parser parser];
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *bundlePath = [appBundle pathForResource:@"report1" ofType:@"txt"];
    NSURL *fileURL = [NSURL fileURLWithPath:bundlePath];
    [parser importFromURL:fileURL];
}

- (IBAction)testReport3:(id)sender
{
    FSTech01Parser *parser = [FSTech01Parser parser];
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *bundlePath = [appBundle pathForResource:@"report2" ofType:@"txt"];
    NSURL *fileURL = [NSURL fileURLWithPath:bundlePath];
    [parser importFromURL:fileURL];
}
@end
